from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
import requests
from .api_enterkomputer import get_drones
from .csui_helper import get_access_token, verify_user, get_client_id , get_data_user
import environ
from django.urls import reverse


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"

class Lab9UnitTest(TestCase):

	def test_lab_9_url_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_drones_api(self):
		response = requests.get('https://www.enterkomputer.com/api/product/drone.json')
		self.assertEqual(response.json(),get_drones().json())




   

    
